# Role Name

This role installs Netbox.

More can be found out about Netbox here: [https://netbox.readthedocs.io]

## Requirements

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

## Role Variables

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

| Variable             | Type   | Required | Default Value | Sample            | Description                                             |
|----------------------|--------|----------|---------------|-------------------|---------------------------------------------------------|
| netbox_secret_key    | string | Yes      |               |                   | Genereate a 50char secret key. See note below           |
| netbox_allowed_hosts | string | Yes      | localhost     | *                 | Specify which hosts are allowed to access this instance |
| netbox_release_check | bool   | No       | True          | True              |                                                         |
| netbox_timezone      | string | No       | UTC           | UTC               | Enter a timezone code here                              |
| psql_dbname          | string | Yes      |               | netbox            |                                                         |
| psql_hostname        | string | Yes      |               | localhost         |                                                         |
| psql_username        | string | Yes      |               | netboxdbuser      |                                                         |
| psql_password        | string | Yes      |               | dbpass1234        |                                                         |
| psql_port            | int    | No       | 5432          | 5432              |                                                         |
| mail_server          | string | No       |               | mail.server.tld   |                                                         |
| mail_port            | int    | No       |               | 587               |                                                         |
| mail_username        | string | No       |               | mail_username     |                                                         |
| mail_password        | string | No       |               | password123       |                                                         |
| mail_ssl             | bool   | No       |               | False             |                                                         |
| mail_tls             | bool   | No       |               | True              |                                                         |
| mail_timeout         | int    | No       |               | 10                |                                                         |
| mail_from            | string | No       |               | netbox@domain.tld |                                                         |
| redis_server         | string | Yes      | localhost     | localhost         | REDIS server URL or IP                                  |
| redis_port           | int    | No       | 6379          | 6379              | REDIS server port                                       |

## Dependencies

This role requres the community.postgresql collection

More information can be found on this collection here: [https://docs.ansible.com/ansible/latest/collections/community/postgresql/index.html]

You can install this using the following command:

    ansible-galaxy collection install community.postgresql

## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: host
      become: True
      gather_facts: True
      roles:
        - role: '/path/to/roles/netbox'

      vars:
        netbox_secret_key:    'ThisIsASampleSecretKey123456789!@$%'
        netbox_allowed_hosts: '*'
        netbox_release_check: True
        netbox_timezone:      'UTC'

        psql_dbname:          "netbox"
        psql_hostname:        "localhost"
        psql_username:        "psqluser"
        psql_password:        "password123"
        psql_port:            '5432'

        mail_server:          "mail.server.tld"
        mail_port:            587
        mail_username:        'mail_username'
        mail_password:        'password123'
        mail_ssl:             False
        mail_tls:             True
        mail_timeout:         10
        mail_from:            'netbox@domain.tld'

        redis_server:         'localhost'
        redis_port:           6379


### Secret key generation

You can use the following command to generate a secret key for {{ netbox_secret_key }}

    tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' </dev/urandom | head -c 50  ; echo

## License

MIT

## Author Information

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
